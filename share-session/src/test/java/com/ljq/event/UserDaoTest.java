package com.ljq.event;

import com.ljq.event.user.UserDao;
import com.ljq.event.user.UserEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @Author linjq
 * @Date 17/7/28 下午5:23
 */

//这是JUnit的注解，通过这个注解让SpringJUnit4ClassRunner这个类提供Spring测试上下文。
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest()
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserDaoTest {

    @Autowired
    private UserDao userDao;

    @Test
    public void saveTest(){
        UserEntity user = new UserEntity();
        user.setName("111");
        userDao.save(user);
    }
}
