package com.ljq.event;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import com.ljq.event.session.SessionFilter;

/**
 * @Author linjq
 * @Date 17/7/28 下午4:34
 */
@SpringBootApplication
public class EventApp {
    static Logger log = Logger.getLogger("aaa");
    public static void main(String[] args) {
        SpringApplication.run(EventApp.class, args);
        log.info("fininsh");

    }

    @Bean
    public EmbeddedServletContainerFactory servletContainer(){
        TomcatEmbeddedServletContainerFactory tom = new TomcatEmbeddedServletContainerFactory();
        //默认设置配置文件的端口，如果这里也设置端口，还是读取配置文件端口
        tom.setPort(8081);
        return tom;
    }

    @Bean
    public FilterRegistrationBean sessionRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean(new SessionFilter());
        registration.addUrlPatterns("/*");
        return registration;
    }


}
