package com.ljq.event.session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @Author linjq
 * @Date 17/7/28 下午11:28
 */
public class HttpServletRequestImpl extends HttpServletRequestWrapper {

    private HttpServletRequest request;

    private HttpServletResponse response;

    public HttpServletRequestImpl(HttpServletRequest request, HttpServletResponse response) {
        super(request);
        this.request = request;
        this.response = response;
    }

    public HttpServletRequestImpl(HttpServletRequest request) {
        super(request);
        this.request = request;
    }

    @Override
    public HttpSession getSession() {
        return new HttpSessionImpl(request,response);
    }
}
