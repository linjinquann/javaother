package com.ljq.event.session;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author linjq
 * @Date 17/7/28 下午11:25
 */
public class SessionFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        filterChain.doFilter(
                new HttpServletRequestImpl(
                        (HttpServletRequest)servletRequest, (HttpServletResponse)servletResponse
                ),
                servletResponse);
    }

    @Override
    public void destroy() {

    }

}
