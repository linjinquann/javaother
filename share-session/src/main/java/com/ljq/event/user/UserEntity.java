package com.ljq.event.user;

import javax.persistence.*;

/**
 * @Author linjq
 * @Date 17/7/28 下午5:06
 */
@Entity
@Table(name = "user1")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
