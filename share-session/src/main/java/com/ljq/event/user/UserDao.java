package com.ljq.event.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Author linjq
 * @Date 17/7/28 下午5:10
 */
@Repository
public interface UserDao extends JpaRepository<UserEntity, Long> {

}
