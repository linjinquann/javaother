package com.ljq.event.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author linjq
 * @Date 17/7/28 下午5:10
 */
@Service
public class UserService {

    @Autowired
    UserDao userDao;

    public void save(UserEntity entity){

        userDao.save(entity);
    }

}
