package com.ljq.event;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import com.ljq.event.session.SessionFilter;

/**
 * @Author linjq
 * @Date 17/7/28 下午4:34
 */
@SpringBootApplication
public class EventApp2 {
    public static void main(String[] args) {
        SpringApplication.run(EventApp2.class, args);
    }

    @Bean
    public EmbeddedServletContainerFactory servletContainer(){
        TomcatEmbeddedServletContainerFactory tom = new TomcatEmbeddedServletContainerFactory();
        //默认设置配置文件的端口，如果这里也设置端口，还是读取配置文件端口
        tom.setPort(8082);
        return tom;
    }

    @Bean
    public FilterRegistrationBean indexFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean(new SessionFilter());
        registration.addUrlPatterns("/*");
        return registration;
    }
}
