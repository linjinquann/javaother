package com.ljq.cxf;

import javax.jws.WebService;

/**
 * @Author linjq
 * @Date 17/7/17 下午1:33
 */
@WebService
public class CXFDemoImpl implements CXFDemo {

    public String sayHello(String foo) {
        return "hello "+foo;
    }

}