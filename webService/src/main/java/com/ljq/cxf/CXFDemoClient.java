package com.ljq.cxf;

import com.ljq.Constant;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

/**
 *
 * 客户端调用服务接口
 * @Author linjq
 * @Date 17/7/17 下午2:01
 */
public class CXFDemoClient {
    public static void main(String args[]) {
        JaxWsProxyFactoryBean factoryBean = new JaxWsProxyFactoryBean();
        factoryBean.setServiceClass(CXFDemo.class);
        factoryBean.setAddress(Constant.API_HOST_CXF);

        CXFDemo cxfDemo = (CXFDemo)factoryBean.create();
        String result = cxfDemo.sayHello(" linjq");
        System.out.println(result);
    }
}
