package com.ljq.cxf;

import javax.jws.WebService;

/**
 * @Author linjq
 * @Date 17/7/17 下午1:32
 */
@WebService
public interface CXFDemo {

    public String sayHello(String foo);
}
