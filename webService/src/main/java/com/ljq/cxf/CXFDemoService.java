package com.ljq.cxf;

import com.ljq.Constant;

import javax.xml.ws.Endpoint;

/**
 * 1. CXF支持通过Web容器发布WebService，
 *      需要配置cxf.xml, web.xml需要配置servlet
 *      如下地址访问：http://localhost:8080/cxf
 *                  http://localhost:8080/cxf/cxfdemo?wsdl
 *
 * 2. 也可以在嵌入式代码中通过发布WebService。
 *    再次CXFDemoClient进行调用ws服务
 * @Author linjq
 * @Date 17/7/17 下午1:58
 */
public class CXFDemoService {
    public static void main(String[] args) {
        CXFDemo cxf = new CXFDemoImpl();
        Endpoint.publish(Constant.API_HOST_CXF, cxf);
        //浏览器访问：http://localhost:8080/cxf?wsdl
    }
}
