package com.ljq.tax;

/**
 * @Author linjq
 * @Date 17/7/17 下午4:20
 */
public class ThrowsDemo {

    static void throwMethod() throws IllegalAccessException {
        throw new IllegalAccessException("demo");
    }

    public static void main(String[] args) {
        try {
            throwMethod();
        } catch (IllegalAccessException e) {
            System.out.println(e.getMessage());
        }

    }
}
