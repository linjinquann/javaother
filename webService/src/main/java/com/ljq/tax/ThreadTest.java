package com.ljq.tax;

/**
 * @Author linjq
 * @Date 17/7/17 下午2:56
 */
public class ThreadTest extends Thread {

    int time;

    public ThreadTest(String name, int time) {
        super(name);
    }

    public static void main(String[] args) {
//        Thread t = new ThreadTest("aaa", 500);
//        t.start();
//        System.out.println(t.getName());

        //t1();

        try {


            char a = 'a';
            int b = 1 + 'k';
            System.out.println(b);
        } finally {
            System.out.println("end");
        }
    }


    static void t1() {
        try {
            System.out.println("hello");
            System.exit(-1);
        } finally {
            System.out.println("good bye");
        }
    }

}
