package com.ljq.cxf;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.xml.ws.Endpoint;

/**
 * @Author linjq
 * @Date 17/7/17 下午1:36
 */
public class CxfTest {

    private static final String ADDRESS = "http://localhost:8080/cxfdemo";
    @Before
    public void setUp() throws Exception {
        CXFDemoImpl demo = new CXFDemoImpl();
        Endpoint.publish(ADDRESS, demo);
        System.out.println("Start success");
    }

    @Test
    public void testSayHello(){

        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(CXFDemo.class);
        factory.setAddress(ADDRESS);
        CXFDemo client = (CXFDemo)factory.create();
        Assert.assertEquals(client.sayHello("foo"), "hello foo");
    }

}
