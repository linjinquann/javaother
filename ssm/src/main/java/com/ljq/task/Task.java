package com.ljq.task;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Task implements ILinjqJob {

	@Scheduled(cron="0/5 * 12-18 * * ? ")   //每10秒执行一次 ，在12-18之间
//	@Scheduled(cron="0 0/5 7-24 * * ? ")   //每5分钟执行一次 ，在7-24之间
	@Override
	public void task() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println("1>>" + simpleDateFormat.format(new Date()));
	}
	
	/**
	 * https://blog.csdn.net/qq_33556185/article/details/51852537
	 *  按顺序依次为
      1  秒（0~59）
      2  分钟（0~59）
      3 小时（0~23）
      4  天（0~31）
      5 月（0~11）
      6  星期（1~7 1=SUN 或 SUN，MON，TUE，WED，THU，FRI，SAT）
      7.年份（1970－2099）
      其中每个元素可以是一个值(如6),一个连续区间(9-12),一个间隔时间(8-18/4)(/表示每隔4小时),一个列表(1,3,5),通配符。由于"月份中的日期"和"星期中的日期"这两个元素互斥的,必须要对其中一个设置?.
       0 0 10,14,16 * * ? 每天上午10点，下午2点，4点
       0 0/30 9-17 * * ?   朝九晚五工作时间内每半小时
       0 0 12 ? * WED 表示每个星期三中午12点
	 */
}
