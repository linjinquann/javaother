package com.ljq.task;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Task3 implements ILinjqJob {

	@Scheduled(cron="0/5 * 13,18 * * ? ")   //每10秒执行一次    在13，18点每5秒执行
	@Override
	public void task() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println("3>>" + simpleDateFormat.format(new Date()));
	}
}
