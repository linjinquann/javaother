package com.ljq.task;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Task2 implements ILinjqJob {

	@Scheduled(cron="0/5 * 11,12 * * ? ")   //每10秒执行一次    
	@Override
	public void task() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println("2>>" + simpleDateFormat.format(new Date()));
	}

}
