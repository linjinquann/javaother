package com.ljq.enums;

public enum MessageEnums {
	SUCCESS(200, "成功", true),
	PARAM_ERROR(300, "参数异常", false);
	
	private MessageEnums(int code, String message, boolean success) {
		this.code = code;
		this.message = message;
		this.success = success;
	}
	
	private int code;
	private String message;
	private boolean success;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
}
