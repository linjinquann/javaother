package com.ljq.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by linjq on 2017/7/26.
 */
@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    public UserEntity findById(long id) {
        return userDao.findById(id);
    }
}
