package com.ljq.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ljq.dto.Result;
import com.ljq.enums.MessageEnums;

/**
 * Created by linjq on 2017/7/26.
 */
@Controller
public class UserController {
	private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private UserService userService;

    @RequestMapping("/test")
    @ResponseBody
    public Result test(){
    	logger.info("UserController.test()");
    	Result result = new Result(MessageEnums.SUCCESS);
    	result.setData("Hello World!" + System.currentTimeMillis());
        return result;
    }
    
    @RequestMapping("/findById")
    @ResponseBody
    public Result findById(){
    	Result result = new Result(MessageEnums.SUCCESS);
    	result.setData(userService.findById(1));
        return result;
    }
}
