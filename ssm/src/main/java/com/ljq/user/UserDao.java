package com.ljq.user;

import org.springframework.stereotype.Repository;

/**
 * Created by linjq on 2017/7/26.
 */
@Repository
public interface UserDao {
    public UserEntity findById(long id);
}
