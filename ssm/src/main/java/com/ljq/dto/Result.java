package com.ljq.dto;

import java.io.Serializable;

import com.ljq.enums.MessageEnums;


public class Result  implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		//响应蚂
		private int code;
		
		//是否成功
		private boolean success;

		//响应消息
		private String message;

		//响应结果
		private Object data;
		
		public Result() {
		}
		public Result(MessageEnums messageEnums) {
			this.code = messageEnums.getCode();
			this.message = messageEnums.getMessage();
			this.success = messageEnums.isSuccess();
		}
		public int getCode() {
			return code;
		}
		public void setCode(int code) {
			this.code = code;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public Object getData() {
			return data;
		}
		public void setData(Object data) {
			this.data = data;
		}
		
		public boolean isSuccess() {
			return success;
		}
		public void setSuccess(boolean success) {
			this.success = success;
		}
		@Override
		public String toString() {
			return "Result [code=" + code + ", success=" + success + ", message=" + message + ", data=" + data + "]";
		}
		
}
