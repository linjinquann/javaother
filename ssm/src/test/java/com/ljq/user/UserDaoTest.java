package com.ljq.user;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * Created by linjq on 2017/7/26.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring-dao.xml"})
public class UserDaoTest {
    Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private UserDao userDao;

    @Test
    public void findById() throws Exception {
        UserEntity user = userDao.findById(1);
        logger.info(user.getName());
        Assert.assertEquals("ljq1",user.getName());
    }
    
    @Test
    public void hello() throws Exception {
        logger.info("helloWorld");
    }

}