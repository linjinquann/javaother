package com.ljq.web;

import com.ljq.common.CommonTest;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author linjq
 * @Date 17/8/27 下午2:58
 */
@SpringBootApplication()
public class WebStart {
    public static void main(String[] args) {
        CommonTest.test();
        //启动引导应用程序
        SpringApplication.run(WebStart.class, args);
    }
}
