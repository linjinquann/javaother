package com.ljq.web.module.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @Author linjq
 * @Date 17/8/27 下午3:39
 */
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping("/save")
    @ResponseBody
    public UserEntity save(@RequestBody UserEntity user) {
        try {
            userService.save(user);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return user;
    }

    @RequestMapping("/findOne")
    @ResponseBody
    public UserEntity findOne(long id) {
        return userService.findOne(id);
    }

    @RequestMapping("/findAll")
    @ResponseBody
    public List<UserEntity> findAll() {
        return userService.findAll();
    }

}
