package com.ljq.web.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
 

public class RestTemplateUtil {
	private static final Log log = LogFactory.getLog(RestTemplateUtil.class);

	private static RestTemplate restTemplate;


	public static ResponseModal post(String url, String requestJSONStr) {

		MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");

		HttpHeaders headers = new HttpHeaders();
//		String encodedToken = "Basic " + Base64.encodeBase64String("admin:123456".getBytes());
//		headers.set("Authorization", encodedToken);
		headers.setContentType(type);
		headers.add("Accept", MediaType.APPLICATION_JSON.toString());
		HttpEntity<String> formEntity = new HttpEntity<String>(requestJSONStr, headers);

		if (restTemplate == null)
			restTemplate = new RestTemplate();

		ResponseModal rm = restTemplate.exchange(url, HttpMethod.POST, formEntity, ResponseModal.class).getBody();
		return rm;
	}
	
	public static ResponseModal postXml(String xml, String requestStr) {

		MediaType type = MediaType.parseMediaType("application/xml; charset=UTF-8");

		HttpHeaders headers = new HttpHeaders();
//		String encodedToken = "Basic " + Base64.encodeBase64String("admin:123456".getBytes());
//		headers.set("Authorization", encodedToken);
		headers.setContentType(type);
		headers.add("Accept", MediaType.APPLICATION_JSON.toString());
		HttpEntity<String> formEntity = new HttpEntity<String>(requestStr, headers);

		// String result = restTemplate.postForObject(url, formEntity,
		// String.class);
		if (restTemplate == null)
			restTemplate = new RestTemplate();

		ResponseModal rm = restTemplate.exchange(xml, HttpMethod.POST, formEntity, ResponseModal.class).getBody();
		return rm;
	}
 
	

}
