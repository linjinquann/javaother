package com.ljq.web.util;

public class Modal {
	

	public Modal() {
		super();
	}

	public Modal(int status, String msg) {
		super();
		this.status = status;	
		this.msg = msg;
	}

	private int status;
	

	
	private String msg;

	public void setSysStatus(SysStatus ss) {
		status=ss.getStatus();
		msg=ss.getMsg();		 
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

 
	
	
	
}
