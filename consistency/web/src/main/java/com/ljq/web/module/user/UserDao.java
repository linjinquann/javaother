package com.ljq.web.module.user;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by linjq on 17/5/30.
 */
public interface UserDao extends JpaRepository<UserEntity, Long> {

}
