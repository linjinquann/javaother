package com.ljq.web.module.user;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @Author linjq
 * @Date 17/8/27 下午3:29
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    UserService userService;

    @Test
    public void saveTest() {
        UserEntity userEntity = new UserEntity();
        userEntity.setName("小明"+System.currentTimeMillis());
        try {
            userService.save(userEntity);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void findOneTest() {
        UserEntity user = userService.findOne(1);
        System.out.println("getOne-->>" + user.getName());
    }

    @Test
    public void findAllTest() {
        List<UserEntity> list = userService.findAll();
        for (UserEntity userEntity : list) {
            System.out.println("findall-->>" + userEntity.getName());
        }
    }

}
