package com.ljq.testweb.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by linjq on 17/5/31.
 */
@Service
public class UserService  {


    @Autowired
    public UserDao dao;

    @Transactional
    public List<UserEntity> saveAll() throws InterruptedException {
        List<UserEntity> list = findAll();
        Thread t;
        for (UserEntity userEntity : list) {
            userEntity.setName(System.currentTimeMillis()+"");
            t = new Thread(new UserThread(userEntity));
        }
        return list;
    }

    public UserEntity findOne(long id){
        return dao.findOne(id);
    }


    public List<UserEntity> findAll(){
        return dao.findAll();
    }

    private class UserThread implements Runnable{
        private UserEntity user;
        public UserThread(UserEntity user){
            this.user = user;
        }

        @Override
        public void run() {
            dao.save(user);
        }
    }
}

