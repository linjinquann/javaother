1. com.ljq.hello
	两种注入方式
2. com.ljq.bean
	1. 单例：
		BeanScopeSingleServiceTest.java
	2. 每次请求创建
		BeanScopePrototypeServiceTest.java
	3. 自动装配 list, map
		BeanAutowriedTest.java

4. com.ljq.contain.BeanAnnotationConfTest.java
	bean注解的使用

5. com.ljq.importresource.ImportResourceConfTest.java
	读取资源文件
	
6. 基于泛型的自动装配
	com.ljq.generic.GenericConfTest.java
	
7 .通知
com.ljq.aop.advice
	around, before, after, afterReturning, afterThrowing
	