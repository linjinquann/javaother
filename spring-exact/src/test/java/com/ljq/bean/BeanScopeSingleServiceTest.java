package com.ljq.bean;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import junit.framework.TestCase;

public class BeanScopeSingleServiceTest {

	private BeanScopeSingeleService service;
	ClassPathXmlApplicationContext context;

	@Before
	public void Before() {
		context = new ClassPathXmlApplicationContext("spring-service.xml");
	}
	
	
	/**
	 * 结果相等，因为是同一容器取出来的单例
	 * 1>2085002312
	 * 2>2085002312
	 */
	@Test
	public void test() {
		service = context.getBean(BeanScopeSingeleService.class);
		int code = service.hashCode();
		System.out.println("1>>>" + code);

		service = context.getBean(BeanScopeSingeleService.class);
		int code2 = service.hashCode();
		System.out.println("2>>>" + code2);
		
		TestCase.assertEquals(code, code2);
	}

	/**
	 * 结果和test1不相等，因为获取了两次容器, test1和test2
	 * 1>2085002312
	 * 2>2085002312
	 */
	@Test
	public void test2() {
		service = context.getBean(BeanScopeSingeleService.class);
		System.out.println("3>" + service.hashCode());
	}

}
