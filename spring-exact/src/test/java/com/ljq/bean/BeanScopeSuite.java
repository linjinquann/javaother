package com.ljq.bean;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BeanScopePrototypeServiceTest.class, BeanScopeSingleServiceTest.class, BeanAutowriedTest.class })
public class BeanScopeSuite {

}
