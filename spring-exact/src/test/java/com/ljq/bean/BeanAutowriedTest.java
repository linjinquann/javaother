package com.ljq.bean;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 测试autowried注入多个
 * @author linjq
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-service.xml")
public class BeanAutowriedTest  {

	@Autowired
	private BeanAutowried beanServiceList;

	/**
	 * @Qualifier 注入指定的实现类
	 */
	@Autowired
	@Qualifier("beanAutoriedOne")
	private IBeanAutoried one;
	@Autowired
	@Qualifier("beanAutoriedTwo")
	private IBeanAutoried two;
	
	
	@Test
	public void test() {
		beanServiceList.say();
		
		System.out.println(one.getClass().getName());
		System.out.println(two.getClass().getName());
	}
}
