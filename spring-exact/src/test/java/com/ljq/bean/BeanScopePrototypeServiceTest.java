package com.ljq.bean;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import junit.framework.Assert;
import junit.framework.TestCase;

public class BeanScopePrototypeServiceTest {


	private BeanScopePrototypeService service;
	private ClassPathXmlApplicationContext context;

	@Before
	public void Before() {
		context = new ClassPathXmlApplicationContext("spring-service.xml");
	}
	
	/**
	 * 结果不相等
	 */
	@Test
	public void test() {
		service = context.getBean(BeanScopePrototypeService.class);
		int code = service.hashCode();
		System.out.println("1>>>" + code);

		service = context.getBean(BeanScopePrototypeService.class);
		int code2 = service.hashCode();
		System.out.println("2>>>" + code2);
		
		TestCase.assertNotSame(code, code2);
	}
	
}
