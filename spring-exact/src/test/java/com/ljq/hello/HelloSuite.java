package com.ljq.hello;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * 测试套件
 * @author linjq
 *
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({IHelloXmlServiceTest.class,IHelloXmlServiceTest2.class})
public class HelloSuite {

}
