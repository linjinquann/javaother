package com.ljq.hello;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ljq.BaseTest;

/**
 * 1. new方式读取xml，获取bean容器，并注入相关实例
 * @author linjq
 * 以下这两个注解其实可以不要，因为已经继承了
 */
public class IHelloXmlServiceTest2 {

	private ApplicationContext ac;
	
	private IHelloXmlServiceImpl iHelloServiceXml;
	
	private IHelloXmlServiceImpl2 iHelloServiceXml2;

	@Before
	public void Before() {
		ac = new ClassPathXmlApplicationContext("classpath:spring-service.xml");
		iHelloServiceXml = ac.getBean(IHelloXmlServiceImpl.class);
		iHelloServiceXml2 = ac.getBean(IHelloXmlServiceImpl2.class);
	}
	
	@Test
	public void test() {
		System.out.println(iHelloServiceXml.say());
		System.out.println(iHelloServiceXml2.say());

	}

}
