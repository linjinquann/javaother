package com.ljq.hello;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ljq.BaseTest;

/**
 * 1. 注解式读取xml，获取bean容器，并注入相关实例
 * @author linjq
 * 以下这两个注解其实可以不要，因为已经继承了
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-service.xml")
public class IHelloXmlServiceTest extends BaseTest{

	@Autowired
	private IHelloXmlServiceImpl iHelloServiceXml;
	
	@Autowired
	private IHelloXmlServiceImpl2 iHelloServiceXml2;

	@Test
	public void test() {
		System.out.println(iHelloServiceXml.say());
		System.out.println(iHelloServiceXml2.say());

	}

}
