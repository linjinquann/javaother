package com.ljq.importresource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-service.xml")
public class ImportResourceConfTest {

	@Autowired
	@Qualifier("driveSource")
	private MyDriverManager myDriverManager;

	@Test
	public void testMyDriverManager() {
		System.out.println(myDriverManager.toString());
	}

}
