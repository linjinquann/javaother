package com.ljq.aop;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ljq.aop.advice.biz.anno.LinjqAnnotationBiz;

/**
 * 注解配置切面
 * @author linjq
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-service.xml")
public class LinjqAnotationAspectTest {
	
	@Autowired
	private LinjqAnnotationBiz linjqAnnotationBiz;
	
	@Test
	public void linjqAnnotation() {
		linjqAnnotationBiz.save("cat");
	}
}
