package com.ljq.aop;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ljq.aop.advice.Fit;
import com.ljq.aop.advice.biz.xml.LinjqXmlBiz;

/**
 * xml配置切面
 * @author linjq
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-aop-schema-advice.xml")
public class LinjqXmlAspectTest {

	@Autowired
	private LinjqXmlBiz linjqXmlBiz;

	@Test
	public void save() {
		System.out.println("=================save");
		linjqXmlBiz.save("dog");
		
	}
	
	/**
	 * 测试异常
	 */
	@Test
	public void saveEx() {
		System.out.println("=================saveEx");
		linjqXmlBiz.saveEx("cat");
	}
	
	@Test
	public void aroundInit() {
		System.out.println("=================arountInit");
		linjqXmlBiz.aroundInit("monkey", System.currentTimeMillis());
	}

	/**
	 * <aop:declare-parents> 
	 * 使用父类的接口和实现
	 */
	@Test
	public void fit() {
		System.out.println("=================fit");
		linjqXmlBiz.save("bird");
		Fit fit = (Fit) linjqXmlBiz;
		fit.filter();
	}
}
