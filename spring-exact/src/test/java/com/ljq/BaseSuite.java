package com.ljq;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.ljq.hello.HelloSuite;
import com.ljq.hello.IHelloXmlServiceTest;
import com.ljq.hello.IHelloXmlServiceTest2;

/**
 * 测试套件
 * @author linjq
 *
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({HelloSuite.class})
public class BaseSuite {

}
