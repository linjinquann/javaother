package com.ljq.contain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-service.xml")
public class BeanAnnotationConfTest {

	@Autowired
	@Qualifier("getBean") //可以用bean的方法名作为id注入
	private BeanEntity getBean;
	
	@Autowired
	@Qualifier("initBeanEntity") //可以用bean的方法名作为id注入
	private BeanEntity initBeanEntity;

	@Autowired
	@Qualifier("init") //可以用bean的方法名作为id注入
	private AppInit appInit;
	@Autowired
	@Qualifier("destroy") //可以用bean的方法名作为id注入
	private AppDestroy destroy;
	
	@Test
	public void test() {
//		ApplicationContext aContext = new ClassPathXmlApplicationContext("classpath:spring-service.xml");
//		BeanEntity bean = (BeanEntity) aContext.getBean("getBean");
		System.out.println(getBean.getClass().getName());
		System.out.println(initBeanEntity.getClass().getName());
		System.out.println(appInit.getClass().getName());
		System.out.println(destroy.getClass().getName());
	}

}
