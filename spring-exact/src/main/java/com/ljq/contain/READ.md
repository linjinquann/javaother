窗口注解
1. @Bean
1.1. @Bean 标识一个用于配置和初始化一个由spring管理的新对象方法，类似于xml配置的<bean>
1.2. @Bean(name="myBean")自定义beanName
1.3. 通常和@Configuration使用
	见BeanAnnotaion.java
	initMethod（启动时）
	destroyMethod（销毁时）
	可以用方法名作为beanId
	@Bean(name="myBean")作为beanId

1.4 @bean默认是单例的
	@Bean
	@Scope("single")
	@Scope("prototype", proxyMode=)

	