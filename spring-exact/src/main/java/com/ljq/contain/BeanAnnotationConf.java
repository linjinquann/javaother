package com.ljq.contain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanAnnotationConf {

	@Bean(initMethod=("init"))
	public AppInit init(){
		return new AppInit();
	}
	
	@Bean(destroyMethod=("destroy"))
	public AppDestroy destroy(){
		return new AppDestroy();
	}
	
	//可以用方法名作为beanid
	@Bean
	public BeanEntity getBean() {
		return new BeanEntity();
	}
	
	 //可以用name自定义beanid
	@Bean(name="initBeanEntity")
	public BeanEntity initBeanEntity() {
		return new BeanEntity();
	}
}

class AppInit {
	public void init() {
		System.out.println("init...");
	}
}

class AppDestroy {
	public void destroy() {
		System.out.println("destroy...");
	}
}