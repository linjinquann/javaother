package com.ljq.generic;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 基于泛型的自动装配
 * @author linjq
 *
 */
@Configuration
public class GenericConf {
	

	@Bean
	public StringStore stringStore() {
		return new StringStore();
	}
	@Bean
	public IntegerStore integerStore() {
		return new IntegerStore();
	}
}
