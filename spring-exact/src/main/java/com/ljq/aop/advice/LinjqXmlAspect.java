package com.ljq.aop.advice;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 * xml方式配置切面
 * @author linjq
 *
 */
public class LinjqXmlAspect {
	
	
	public void before() {
		System.out.println("Before.");
	}
	
	public void beforeWithParam(String arg) {
		System.out.println("BeforeWithParam." + arg);
	}
	
	public void beforeWithAnnotaion(LinjqAnnotationMethod moocMethod) {
		System.out.println("BeforeWithAnnotation." + moocMethod.value());
	}
	
	
	/**
	 * @param retVal 如果xml没配置，这里也获取不到，和xml的参数名一致
	 */
//	public void afterReturning() {
	public void afterReturning(Object retVal) {
		System.out.println("AfterReturning : " + retVal);
	}
	
	public void afterThrowing(RuntimeException e) {
		System.out.println("AfterThrowing : " + e.getMessage());
	}
	
	/**
	 * 相当于,finally， 肯定会执行
	 */
	public void after() {
		System.out.println("After.");
	}

	/**
	 * 方法前方法后执行
	 * 返回值也可以获取 
	 * 方法出异常没法执行方法后，需要try catch
	 * @param pjp
	 * @return
	 * @throws Throwable
	 */
	public Object around(ProceedingJoinPoint pjp) throws Throwable {
		System.out.println("Around 1.");
		Object obj = pjp.proceed();
		System.out.println("Around 2.");
		System.out.println("Around : " + obj);
		return obj;
	}
	
	public Object aroundInit(ProceedingJoinPoint pjp, String name, long times) throws Throwable {
		System.out.println("Around Init ：" + name + "===" + times);
		System.out.println("Around Init 1.");
		Object obj = pjp.proceed();
		System.out.println("Around Init 2.");
		System.out.println("Around Init : " + obj);
		return obj;
	}

}
