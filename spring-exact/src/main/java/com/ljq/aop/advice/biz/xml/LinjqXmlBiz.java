package com.ljq.aop.advice.biz.xml;

public class LinjqXmlBiz {
	
	public String save(String arg) {
		System.out.println("linjq save : " + arg);
//		throw new RuntimeException(" Save failed!");
		return " Save success!";
	}
	
	public String saveEx(String arg) {
		int i = 1/0;
		System.out.println("linjq save : " + arg);
//		throw new RuntimeException(" Save failed!");
		return " Save success!";
	}
	
	public void aroundInit(String bizName, long times) {
		System.out.println(bizName + "---" + times);
	}

}
