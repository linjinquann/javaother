package com.ljq.aop.advice.biz.anno;

import org.springframework.stereotype.Service;

@Service
public class LinjqAnnotationBiz {
	
//	@LinjqAnnotationMethod("LinjqAnnotationMethod save with method.")
	public String save(String arg) {
		System.out.println("linjqAnnotation save : " + arg);
//		throw new RuntimeException(" Save failed!");
		return " Save success!";
	}

}
