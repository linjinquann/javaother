package com.ljq.importresource;

public class MyDriverManager {
	
	private String url;
	private String username;
	private String password;
	
	public MyDriverManager(String url, String username, String password) {
		super();
		this.url = url;
		this.username = username;
		this.password = password;
	}
	
	@Override
	public String toString() {
		return "ImportResource [url=" + url + ", username=" + username + ", password=" + password + "]";
	}
}
