package com.ljq.importresource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:spring-config.xml")
public class ImportResourceConf {

	@Value("${url}")
	private String url;
	
	@Value("${username}")
	private String username;
	
	@Value("${password}")
	private String password;

	@Bean(name = "driveSource")
	public MyDriverManager getDriveSource() {
		return new MyDriverManager(url, username, password);
	}
}
