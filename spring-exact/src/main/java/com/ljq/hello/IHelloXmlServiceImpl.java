package com.ljq.hello;

import org.springframework.stereotype.Service;

@Service
public class IHelloXmlServiceImpl implements IHelloXmlService {

	public String say() {
		return "hello world";
	}

}
