package com.ljq.hello;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.ljq.hello.HelloPrinter;
import com.ljq.hello.IHelloService;

/**
 * 3. 全注解式创建实现类
 * @author linjq
 */
@Configuration
@ComponentScan
public class HelloAppConf {

	// 创建实现
	@Bean
	IHelloService mockMessageService() {
		return new IHelloService() {
			public String say() {
				return "Hello World";
			}
		};
	}

	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(HelloAppConf.class);
		HelloPrinter printer = context.getBean(HelloPrinter.class);
		printer.printMessage();
	}

}
