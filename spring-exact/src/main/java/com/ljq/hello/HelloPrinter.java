package com.ljq.hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HelloPrinter {
	final private IHelloService service;

	@Autowired
	public HelloPrinter(IHelloService service) {
		this.service = service;
	}

	public void printMessage() {
		System.out.println(this.service.say());
	}
}
