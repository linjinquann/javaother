package com.ljq.bean;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope(scopeName=ConfigurableBeanFactory.SCOPE_PROTOTYPE) //原型，每次请求重新创建
public class BeanScopePrototypeService {

	public void say() {
		System.out.println("scope single");
	}
}
