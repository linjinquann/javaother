package com.ljq.bean;


import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BeanAutowried {
	
	//可以注入接口的两个实例
	@Autowired
	private List<IBeanAutoried> list;
	
	@Autowired
	private Map<String, IBeanAutoried> map;
	
	
	public void say() {
		System.out.println("---list---");
		for (IBeanAutoried iBeanService : list) {
			System.out.println(iBeanService.getClass().getName());
		}

		System.out.println("\r\n---map---");
		Iterator<String> iterator = map.keySet().iterator();
		while (iterator.hasNext()) {
			String string = (String) iterator.next();
			System.out.println(map.get(string).getClass().getName());
		}
	}
}
