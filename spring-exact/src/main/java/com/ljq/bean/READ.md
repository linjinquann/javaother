bean

配置项
	id bean唯一标识
	class 哪个类
	scope 作用域
		single 单例
		prototype 每次请求创建新实例,destroy不生效
		request 每次请求创建，仅在当前request有效
		session 每次请求创建，仅在当前session内有效
		global session 
 		
	construct params
	properties 属性
	autowring mode 自动装配
	lazy-init  懒加载
	init/destruction  初始化/销毁
作用域
生命周期
自动装配
resource&resourceLoader