package com.ljq.bean;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope(scopeName=ConfigurableBeanFactory.SCOPE_SINGLETON) //默认就是单例，只生成一次
public class BeanScopeSingeleService {

	public void say() {
		System.out.println("scope single");
	}
}
