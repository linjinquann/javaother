一、线程
com.ljq.army
com.ljq.energy
com.ljq.facetest
com.ljq.mantou

线程创建
Thread()
Thread(String name)
Thread(Runable target)
Thread(Runable target, String name)

线程的方法
void start()	#启动线程
static void sleep(long millis)	#线程休眠
static void sleep(long millis, int nanos)
void join()	#使其它线程等待当前线程终止
void join(long millis)	#同上
void join(long millis, int nanos)	#同上
static void yield()	#当前运行线程释放处理器资源

获取线程引用
static Thread CurrentThread()

错误线程停止方法：
	stop、interrupt。
正确线程停止方法：
	使用退出旗标

线程的互斥与同步
	关键数据同一时间只能被同一线程访问
	实现 synchroinized(instansic lock)
	
同步：生产者消费者
	同步的实现 wait/notify/notifyAll()

总结
1.创建
2.volatile关键字，保证线程可以正确的读取其它线程写入的值 
3.争用条件
4.互斥 synczte
5.同步

扩展及展望
java memory mode
locks& condition对象
线程安全性
常用交互模型
	生产消费
	读写锁
	future模型
	work thread


二、socket, tcp/ip
com.ljq.socket

三、面试题，spring, hibernate, 接口，抽象
com.ljq.yy

四、链表
com.ljq.yy.data.singlelinkedlist

五、单例
com.ljq.yy.single

