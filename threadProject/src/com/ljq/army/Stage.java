package com.ljq.army;

/**
 * 战场
 * @author linjq
 */
public class Stage extends Thread{
	
	
	@Override
	public void run() {
		ArmyRunable left = new ArmyRunable();
		ArmyRunable right = new ArmyRunable();
		
		Thread tleft = new Thread(left, "汉朝军队>>>>");
		Thread tright = new Thread(right, "魏国军队");
		
		tleft.start();
		tright.start();
		
		try {
			//休眠看军队厮杀
			Thread.sleep(50);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("半路杀出了关羽");
		Thread mrGuan = new Hero();
		mrGuan.setName("关羽");
		
		left.keepRuning = false;
		right.keepRuning = false;
		
		try {
			//休眠
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		mrGuan.start();
		
		//所有线程等待，所以当前线程等待英雄线程执行完再执行。
		try {
			mrGuan.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("灭了魏军，谢谢观看，再见");
		
	}
	
	public static void main(String[] args) {
		new Stage().start();
	}

}
