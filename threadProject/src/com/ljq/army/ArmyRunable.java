package com.ljq.army;


/**
 * 军队线程
 * @author linjq
 *
 */
public class ArmyRunable  implements Runnable{

	//保证线程可以正确的读取其它线程写入的值 
	volatile boolean keepRuning = true;
	
	@Override
	public void run() {
		String name = Thread.currentThread().getName();
		while (keepRuning) {
			for(int i=0; i<5; i++) {
				
				System.out.println(name + "进攻对方：" + i);
				//让出处理时间
				Thread.yield();
				
			}
			
		}
		System.out.println(name + "结束了战斗");
		
	}
	

}
