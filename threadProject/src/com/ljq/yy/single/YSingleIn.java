package com.ljq.yy.single;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.CountDownLatch;


public class YSingleIn {

	//1.私有构造方法
	private YSingleIn() {
		
	}
	
	//静态内部类
	private final static class YSingleInner{
		//创建实例
		private final static YSingleIn instance = new YSingleIn();
	}

	//获取静态内部实例
	public static YSingleIn getInstance() {
		return YSingleInner.instance;
	}
	
	public void say() {
		System.out.println("hello world in");
	}
	
}
