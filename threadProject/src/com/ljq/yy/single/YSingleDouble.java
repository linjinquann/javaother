package com.ljq.yy.single;

/**
 * 双重锁
 * 
 * @author linjq
 *
 */
public class YSingleDouble {

	// 1.私有构造方法
	private YSingleDouble() {
	}

	private static YSingleDouble instance;

	public static YSingleDouble getInstance() {

		if (null == instance) {
			synchronized (YSingleDouble.class) {
				if (null == instance) {
					instance = new YSingleDouble();
				}
			}
		}
		return instance;
	}
	
	public void say() {
		System.out.println("hello world double");
	}

}
