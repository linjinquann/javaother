package com.ljq.yy.data.singlelinkedlist;

/**
 * 老师需要找某一个同学，但是碰巧实在放假的时间内。同学们所处的位置都不一样。老师知道班长的手机号码，所以老师打电话给了班长，班长说他也不知道，但是他知道'我'的电话，他又打给我，我也不知道那位同学的地址，我又继续向下一个同学打电话，直到找到他。
 * 那么在以上示例中，加入每一个同学都有另一个同学的电话（有且仅有一个）。
 * 我们就可以说符合单向链表的环境了。大家可以理解记忆。
 * 单向链表测试
 * @author linjq
 */
public class SingleLinkListTest {
	
	public static void main(String agrs[]) {
//		SingleLinkedList linkList = new SingleLinkedList();
		DoubleLinkedList linkList = new DoubleLinkedList();
		int n = 10;
		try {
			for (int i = 0; i < n; i++) {
				linkList.insert(i, new Integer(i + 1));
			}
			linkList.insert(1, new Integer(200));
			linkList.delete(2);
			for (int i = 0; i < linkList.size(); i++) {
				Node node = linkList.getNode(i);
				System.out.print(linkList.getData(i) + node.toString() +" --> " );
				
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}
}
