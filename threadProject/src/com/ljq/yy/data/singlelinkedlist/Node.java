package com.ljq.yy.data.singlelinkedlist;

/**
 * 单向链表：当前对像持有下一对象的引用
 * 双向链表：当前对象持有上下对象的引用
 * 
 * @author linjq
 *
 */
public class Node {

	private Object element;
	private Node prev; // 上一对象的引用，双向链表需要有持有上一对象引用
	private Node next; // 下一对象的引用

	public Object getElement() {
		return element;
	}

	public void setElement(Object element) {
		this.element = element;
	}

	public Node getNext() {
		return next;
	}

	public void setNext(Node next) {
		this.next = next;
	}

	
	public Node getPrev() {
		return prev;
	}

	public void setPrev(Node prev) {
		this.prev = prev;
	}

	public Node(Object element, Node next) {
		super();
		this.element = element;
		this.next = next;
	}

	public Node(Node next) {
		this.next = next;
	}

	
	
	public Node(Object element, Node next, Node prev) {
		super();
		this.element = element;
		this.prev = prev;
		this.next = next;
	}

	public Node() {

	}

	@Override
	public String toString() {
		return "【element=" + element+"】";
	}
	
	
}
