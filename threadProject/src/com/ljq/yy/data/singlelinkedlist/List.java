package com.ljq.yy.data.singlelinkedlist;

public interface List {

	void insert(int i, Object o) throws Exception;

	void delete(int i) throws Exception;

	Object getData(int i) throws Exception;
	
	Node getNode(int i) throws Exception;

	int size() throws Exception;

}
