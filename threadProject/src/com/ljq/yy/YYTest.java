package com.ljq.yy;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * 解释List<? extends T>和List<? super T>
int 和integer的区别
	integer是int的包装类，
	int的变量不需要实例化, Interger的变量需要实例化.
	int的默认值为0,  Integer的默认值为null.
	itn 只能计算， integer可以调用很多对应的方法
java线程
	互斥：synchroinized
	同步：wait/notify/notifyall
	volicat关键字，保证线程可以正确的读取其它线程写入的值
	 
spring
	1. spring是什么：Spring是一个轻量级的控制反转（IoC机制）和面向切面（AOP）的容器框架。
	2. 它的特性：
		轻量：从大小与开销两方面而言Spring都是轻量的。
		非侵入式的：使用Spring,我们的类还是pojo类，完全不用继承和实现Spring的类和接口等。
	3. 它的作用
		ioc机制（ioc控制反转， di依赖注入）
			IOC控制反转：说的是创建对象实例的控制权从代码控制剥离到IOC容器控制，实际就是你在xml文件控制，侧重于原理，全部交给spring管理。
			DI依赖注入：不是我们自己控制对象从容器中查找依赖，而是容器在对象初始化时不等对象请求就主动将依赖传递给它，侧重于实现。
			它们是spring核心思想的不同方面的描述。
	   	aop
	   		是什么:aop面向切面编程，aop是oop的一个补充
	   			oop引入封装、继承、多态等特性模拟一种上下层次的关系。当我们要为分散的对象引入公共行为的时候，oop就没办法做到，oop只能描述从上到下，不能描述从左到右
	   			例如日志功能
	   		作用：
	   			日志管理，权限，事务

hibernate框架
	1. 是什么：orm：
		hibernate是一个ORM,  即是对象关系映射框架，用于对象与关系数据库表记录的转换，是用来操作数据库的。
	2. 作用：
		它把数据库中的表，转换成java类，通过xml文件或者注解来实现类和表之间的映射。
	这样的好处在于，可以面向对象的思想来操作数据库。。。
		
抽象基类和接口的用途场景
	1 .接口
		什么是接口：
			接口就是一些方法特征的集合------接口是对抽象的抽象
			通过接口可以实现不相关类的相同行为，而不需要了解对象所对应的类
		接口的特性
			只能包含抽象方法
			静态常量属性
			
	2. 抽象基类
		什么是抽象
			抽象类对某具体类型的部分实现------抽象类是对具体的抽象。
		抽象的特性
			抽象、普通方法
			可以定义普通属性，也可以定义静态常量属性
			
		
	3. 使用场景
			1. 接口是核心，其定义了要做的事情，包含了许多的方法，但没有定义这些方法应该如何做。
			2. 如果许多类实现了某个接口，那么每个都要用代码实现那些方法 
			3. 如果某一些类的实现有共通之处，则可以抽象出来一个抽象类，让抽象类实现接口的公用的代码，而那些个性化的方法则由各个子类去实现。 
			
			总结：抽象类是为了简化接口的实现，他不仅提供了公共方法的实现，让你可以快速开发，又允许你的类完全可以自己实现所有的方法，不会出现紧耦合的问题。 
			场景：
				1 优先定义接口 
				2 如果有多个接口实现有公用的部分，则使用抽象类，然后集成它。 
		
		
单例模式
懒汉(延迟加载)，饿汉， 双重锁(延迟加载)， 静态内部类(延迟加载), 枚举（防止反序列化）

单链表,双链表,数据结构和操作系统,计算机网络都是必须考的
 */
public class YYTest {
	
	public static void main(String[] args) {
		extendList();
	}
	
	/**
	 * List<？ extends T >是T的某种子类的一个集合，
	 * 所以List中的方法的参数涉及到通配符的都不能被调用。
	 * 因为编译器不知道集合具体装的是T的哪个子类对象（subclass） ，
	 * 所以不可以向集合中添加T的子类对象，包括T对象
	 */
	public static void extendList(){
		
		List<? extends YYEntity> list = new ArrayList<>();
		YYEntity e = new YYEntity();
//		list.add(e);
		
	}
	
	public static void superList() {

		List<? super YYEntity> list = new ArrayList<>();
		YYEntity e = new YYEntity();
		YYEntitySub e1 = new YYEntitySub();
		list.add(e);
		list.add(e1);
	}
}

