package com.ljq.cvte;

public class TestPrime {
	public static void prime(int num) {
		//2    3    5    7    11    13    17    19    23 
		//只能被1和自己整除的数为素数
		for(int i=2; i<num;i++) {
			
			boolean flag = true;
			for(int j=2; j<i; j++) {
				if(i%j==0) {
					flag = false;
					break;
				}
			}
			
			if(flag) {
				System.out.print("   " + i);				
			}
		}
	}

	public static void main(String[] args) {
		//列出1-1000的质数
		prime(1000);
		
	}
}