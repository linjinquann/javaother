package com.ljq.alx;

import java.util.Collections;
import java.util.LinkedHashMap;

public class TestAlx {
	/**
	 * 1nf，属性不可分
	 * 2nf,  唯一主键
	 * 3nf, 3NF是对字段冗余性的约束，即任何字段不能由其他字段派生出来，它要求字段没有冗余
	 * BCNF：符合3NF，并且，主属性不依赖于主属性。
	 	若关系模式属于第二范式，且每个属性都不传递依赖于键码，则R属于BC范式。
	 */

	public static void main(String[] args) {



		//
//		new SubClazz(3).printVal();


		//TestClazz<? extends Collections> foo = new TestClazz<LinkedHas>();
		Integer i = 42;
		Double d = 42.0d;
		Long l = 42l;
		System.out.println(i.equals(42));
		System.out.println(d.equals(i));
		System.out.println(d.equals(l));
	}
}


class Clazz {
	protected int i = 1;
	public Clazz() {
		this(2);
	}
	public Clazz(int i) {
		this.i = i;
	}
	public void printVal() {
		System.out.println(this.i);
	}
	
}

class SubClazz extends Clazz {
	public SubClazz() {
	}
	public SubClazz(int i) {
		super(i);
	}
}

class TestClazz<T> {

}