package com.ljq.mantou;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

//测试
public class ProducerMain {

	public static void main(String[] args) {
		BlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>(100);
		Producer1 p = new Producer1(queue);
		Consumer1 c1 = new Consumer1(queue);
		Consumer1 c2 = new Consumer1(queue);

		new Thread(p).start();
		new Thread(c1).start();
		new Thread(c2).start();
	}
}

class Producer1  implements Runnable {
	private final BlockingQueue<Integer> queue;

	public Producer1(BlockingQueue q) {
		this.queue = q;
	}

	@Override
	public void run() {
		try {
			while (true) {
				Thread.sleep(1000);// 模拟耗时
				queue.put(produce());
			}
		} catch (InterruptedException e) {

		}
	}

	private int produce() {
		int n = new Random().nextInt(10000);
		System.out.println("Thread:" + Thread.currentThread().getId() + " produce:" + n);
		return n;
	}
}

// 消费者
class Consumer1 implements Runnable {
	private final BlockingQueue<Integer> queue;

	public Consumer1(BlockingQueue q) {
		this.queue = q;
	}

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(2000);// 模拟耗时
				consume(queue.take());
			} catch (InterruptedException e) {

			}

		}
	}

	private void consume(Integer n) {
		System.out.println("Thread:" + Thread.currentThread().getId() + " consume:" + n);

	}
}
