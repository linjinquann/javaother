/**
 * 
 */
package com.ljq.jdom;

import junit.framework.TestCase;

/**
 * @author ljq
 *	2015年11月24日下午4:39:14
 */
public class JDomDemoTest extends TestCase {
	static String fileName = "src/resource/jdomDemo.xml";
	public void testCreateXml(){
		JDomDemo jd  = new JDomDemo();
		jd.createXml(fileName);
	}
	public void testParseXml(){
		JDomDemo jd = new JDomDemo();
		jd.parserXml(fileName);
	}
}
