/**
 * 
 */
package com.ljq.sax;

import com.ljq.dom.DomDemo;
import junit.framework.TestCase;

/**
 * @author ljq
 *	2015年11月24日下午4:22:32
 */
public class SaxDemoTest extends TestCase {
	static String fileName = "src/resource/SaxDemo.xml";
	public void testCreateXml() {
		DomDemo dd = new DomDemo();
		dd.init();
		dd.createXml(fileName);
	}
	public void testParseXml() {
		SaxDemo sd = new SaxDemo();
		sd.parserXml(fileName);
	}
}
