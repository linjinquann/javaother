/**
 * 
 */
package com.ljq.dom4j;

import junit.framework.TestCase;

/**
 * @author ljq
 *	2015年11月24日下午4:29:54
 */
public class Dom4jDemoTest extends TestCase {
	static String fileName = "src/resource/dom4jDemo.xml";
	public void testCreateXml(){
		Dom4jDemo dj = new Dom4jDemo();
		dj.createXml(fileName);
	}
	public void testParseXml(){
		Dom4jDemo dj = new Dom4jDemo();
		dj.parserXml(fileName);
	}
	
}
