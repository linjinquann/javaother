/**
 * 
 */
package com.ljq.dom;

import junit.framework.TestCase;

/**
 * @author ljq
 *	2015年11月24日下午4:01:02
 */
public class DomDemoTest extends TestCase {
	static String fileName = "src/resource/DomDemo.xml";
	
	public void testCreateXml(){
		DomDemo dd = new DomDemo();
		dd.init();
		dd.createXml(fileName);
	}
	public void testParseXml(){
		DomDemo dd = new DomDemo();
		dd.init();
		dd.parserXml(fileName);
	}
}
