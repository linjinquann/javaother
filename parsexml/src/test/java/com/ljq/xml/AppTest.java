package com.ljq.xml;

import com.ljq.dom.DomDemoTest;
import com.ljq.dom4j.Dom4jDemoTest;
import com.ljq.jdom.JDomDemoTest;
import com.ljq.sax.SaxDemoTest;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


/**
 * Unit test for simple App.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({

        DomDemoTest.class,
        Dom4jDemoTest.class,
        JDomDemoTest.class,
        SaxDemoTest.class


}) //可以添加多个单元测试，同一执行，以逗号分隔
public class AppTest  {

    @BeforeClass //全局只会执行一次，而且是第一个运行
    public static void beforeClass() {
        System.out.println("beforeClass invoked.");
    }

    @Before  //在测试方法运行之前运行
    public void setUp() {
        System.out.println("初始化");
    }

    /**
     *  测试方法
     * @Test注解提供2个参数
     * “expected”，定义测试方法应该抛出的异常，如果测试方法没有抛出异常或者抛出了一个不同的异常，测试失败
     * “timeout”，如果测试运行时间长于该定义时间，测试失败（单位为毫秒）
     */
    @Test
    public void testAdd() {
    }

    @Test
    public void testAdd1() {
    }

    @After //在测试方法运行之后允许
    public void tearDown() {
        System.out.println("测试退出");
    }

    @AfterClass //全局只会执行一次，而且是最后一个运行
    public static void afterClass() {
        System.out.println("afterClass invoked.");
    }

}
