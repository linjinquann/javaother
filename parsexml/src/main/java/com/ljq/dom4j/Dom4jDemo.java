/**
 *
 */
package com.ljq.dom4j;

/**
 * DOM4J生成和解析XML文档
 *
 * @author ljq
 * 2015年11月24日下午4:25:37
 */

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.dom4j.*;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.ljq.xml.XmlDocument;

/**
 * @author hongliang.dinghl Dom4j 生成XML文档与解析XML文档
 */
public class Dom4jDemo implements XmlDocument {

    public void createXml(String fileName) {
        Document document = DocumentHelper.createDocument();

        // 创建根节点
        Element employees = document.addElement("employees");

        //创建子节点
        Element employee = employees.addElement("employee");
        Element name = employee.addElement("name");
        name.setText("ddvip");
        Element sex = employee.addElement("sex");
        sex.setText("m");
        Element age = employee.addElement("age");
        age.setText("29");
        Element createTime = employee.addElement("createTime");
        createTime.setText(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));

        //创建子节点
        Element otherInfo = employees.addElement("otherInfo");
        otherInfo.setText("otherInfo");
        otherInfo.addAttribute("version", "1.0"); //添加属性

        try {
            Writer fileWriter = new FileWriter(fileName);
//			XMLWriter xmlWriter = new XMLWriter(fileWriter);  //可省略不输出字符编码
            OutputFormat format = new OutputFormat();
            format.setEncoding("utf-8");
            XMLWriter xmlWriter = new XMLWriter(fileWriter, format);
            xmlWriter.write(document);
            xmlWriter.close();
        } catch (IOException e) {

            System.out.println(e.getMessage());
        }

    }

    public void parserXml(String fileName) {
        File inputXml = new File(fileName);
        SAXReader saxReader = new SAXReader();
        try {
            Document document = saxReader.read(inputXml);
            Element employees = document.getRootElement();


            Element otherInfo = employees.element("otherInfo");
            System.out.println("otherInfo Node>>>>>" + otherInfo.getText());

            for (Iterator i = employees.elementIterator(); i.hasNext(); ) {
                Element employee = (Element) i.next();
                if (employee.getName().equals("otherInfo")) {
                    Iterator it = employee.attributeIterator();
                    String text = employee.getText();
                    System.out.println("otherInfo>>>" + text);

                    while (it.hasNext()) {
                        Attribute attribute = (Attribute) it.next();
                        String attr=attribute.getText();
                        System.out.println( attribute.getName() + ">>>"+attr);
                    }
                }

                for (Iterator j = employee.elementIterator(); j.hasNext(); ) {
                    Element node = (Element) j.next();
                    System.out.println(node.getName() + ":" + node.getText());
                }
            }
        } catch (DocumentException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("dom4j parserXml");
    }
}