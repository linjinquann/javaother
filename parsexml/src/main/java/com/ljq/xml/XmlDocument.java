package com.ljq.xml;

/**
 * SAX是基于事件流的解析,DOM是基于XML文档树结构的解析
 * @author hongliang.dinghl 定义XML文档建立与解析的接口
 */
public interface XmlDocument {
	/**
	 * 建立XML文档
	 * @param fileName
	 *            文件全路径名称
	 */
	public void createXml(String fileName);

	/**
	 * 解析XML文档
	 * @param fileName
	 *            文件全路径名称
	 */
	public void parserXml(String fileName);
}
