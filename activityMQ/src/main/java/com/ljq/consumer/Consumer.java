package com.ljq.consumer;

public interface Consumer {

	public void init();
	
	public void getMessage(String name);
}
