package com.ljq.consumer;

import javax.jms.JMSException;  
import javax.jms.Message;  
import javax.jms.MessageListener;  
import javax.jms.TextMessage;

public class ConsumerListener1  implements MessageListener{

	@Override
	public void onMessage(Message message) {
		try {  
            System.out.println("订阅一收到消息："+((TextMessage)message).getText());  
        } catch (JMSException e) {  
            e.printStackTrace();  
        }  
		
	}  
}  
