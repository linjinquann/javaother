package com.ljq.p2p;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import com.ljq.consumer.ConsumerListener;  
  
/**
 * 在Point-To-Point  点对点通信模式中，消息只要没有被消费，一直是挂起的状态，直到被消费者消费。
 */
public class Receiver {  
    public static void main(String[] args) {  
        // ConnectionFactory ：连接工厂，JMS 用它创建连接  
        ConnectionFactory connectionFactory;  
        // Connection ：JMS 客户端到JMS Provider 的连接  
        Connection connection = null;  
        // Session： 一个发送或接收消息的线程  
        Session session;  
        // Destination ：消息的目的地;消息发送给谁.  
        Destination destination;  
        // 消费者，消息接收者  
        MessageConsumer consumer;  
        connectionFactory = new ActiveMQConnectionFactory(  
                ActiveMQConnection.DEFAULT_USER,  
                ActiveMQConnection.DEFAULT_PASSWORD, "tcp://localhost:61616");  
        try {  
            // 构造从工厂得到连接对象  
            connection = connectionFactory.createConnection();  
            // 启动  
            connection.start();  
            // 获取操作连接  
            session = connection.createSession(Boolean.FALSE, Session.AUTO_ACKNOWLEDGE);  
//            session = connection.createSession(Boolean.FALSE, Session.CLIENT_ACKNOWLEDGE);   //如果是这种模式需要手动应答message.acknowledge()
            // 获取session注意参数值xingbo.xu-queue是一个服务器的queue，须在在ActiveMq的console配置  
            destination = session.createQueue("FirstQueue");  
            consumer = session.createConsumer(destination);  
            
            //1.一直监听，循环等待
//            while (true) {  
//                // 设置接收者接收消息的时间，为了便于测试，这里谁定为100s  
//                TextMessage message = (TextMessage) consumer.receive(100000);  
//                if (null != message) {  
//                    System.out.println("收到消息" + message.getText());
//                } else {  
//                    break;  
//                }  
//            }  
            
          //2.使用监听器监听发送过来的消息  
            consumer.setMessageListener(new ConsumerListener());  
            Thread.currentThread().sleep(1000*3);
        } catch (Exception e) {  
            e.printStackTrace();  
        } finally {  
            try {  
                if (null != connection)  
                    connection.close();  
            } catch (Throwable ignore) {  
            }  
        }  
    }  
}
