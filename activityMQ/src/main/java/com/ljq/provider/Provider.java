package com.ljq.provider;

public interface Provider {
	
	public void init();
	
	public void sendMessage(String name);
	

}
