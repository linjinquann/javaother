package com.ljq.consumer;

import org.junit.Test;


public class ConsumerImplTest {

	@Test
	public void test() {
		Consumer consumer = new ConsumerImpl();
		consumer.init();
		consumer.getMessage("linjq-1");
	}

	/**
	 * 多线程，用juint没办法进入生产消息，不知原因
	 * @param args
	 */
	public static void main(String[] args) {
		Consumer consumer = new ConsumerImpl();
		consumer.init();
		new Thread(new ThreadConsumer(consumer)).start();
		new Thread(new ThreadConsumer(consumer)).start();
	}

}

class ThreadConsumer implements Runnable {

	private Consumer consumer;

	public void setProvider(Consumer consumer) {
		this.consumer = consumer;
	}

	public ThreadConsumer(Consumer consumer) {
		this.consumer = consumer;
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			consumer.getMessage("linjq-2");
		}
	}

}