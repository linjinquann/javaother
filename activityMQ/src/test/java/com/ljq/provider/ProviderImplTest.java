package com.ljq.provider;

import org.junit.Test;

public class ProviderImplTest {

	@Test
	public void test() {
		
		//单线程
		Provider provider = new ProviderImpl();
		provider.init();
		provider.sendMessage("linjq-1");
	}
	
	
	/**
	 * 多线程，用juint没办法进入生产消息，不知原因
	 * @param args
	 */
	public static void main(String[] args) {
		Provider provider = new ProviderImpl();
		provider.init();
		new Thread(new ThreadProvider(provider)).start();
		new Thread(new ThreadProvider(provider)).start();
		new Thread(new ThreadProvider(provider)).start();
	}

}

class ThreadProvider implements Runnable {
	
	private Provider provider;

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}


	public ThreadProvider(Provider provider) {
		this.provider = provider;
	}

	
	public void run() {
		while(true) {
			provider.sendMessage("linjq-2");
		}
	}
}

